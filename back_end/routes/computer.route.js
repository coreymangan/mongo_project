// computer.route.js

const express = require('express');
const router = express.Router();

const computerController = require('../controllers/computer.controller');

// CRUD
router.post('/createcomputer', computerController.create);
router.get('/getcomputer/:computer_id', computerController.getOne);
router.get('/getcomputers', computerController.getMany);
router.put('/updatecomputer/:computer_id', computerController.update);
router.delete('/removecomputer/:computer_id', computerController.remove);

// Map reduce
router.get('/numbrands', computerController.mapReduceBrandCount);

module.exports = router;