// app.js

const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser =  require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const port = '3000';
const computerRoute = require('./routes/computer.route');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(cors());

// Connecting to DB
mongoose.connect('mongodb://localhost:27017/computer');

// Exits app if error connecting to DB
mongoose.connection.once('error', () => {
    console.log('Error connecting to dataabse. Exiting now...');
    process.exit();
});

mongoose.connection.once('open', () => {
    console.log('Successfuly connected to database.');
});

// route
app.use('/api', computerRoute);


// Listening to port
app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});