// computer.controller.js

const computerModel = require('../models/computer.model');

// Create computer
const create = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    const body = {
        brand: req.body.brand,
        model: req.body.model,
        processor: req.body.processor,
        processor_speed: req.body.processor_speed,
        screen_size: req.body.screen_size,
        screen_resolution: req.body.screen_resolution,
        ram: req.body.ram,
        hdd: req.body.hdd,
        ssd: req.body.ssd,
        graphics_card: req.body.graphics_card,
        operating_system: req.body.operating_system,
        available_location: req.body.available_location
    };

    computerModel.create(body)
        .then(doc => {
            console.log(doc);
            res.send(doc);
        })
        .catch(err => {
            console.log(err);
            res.send({error: err});
        });
};
module.exports.create = create;

// Get one computer
const getOne = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    const id = req.params.computer_id;

    computerModel.findById(id)
        .exec()
        .then(doc => {
            res.send(doc);
        })
        .catch(err => {
            res.send({error: err});
        });
}
module.exports.getOne = getOne;

// Get many computers
const getMany = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    computerModel.find()
        .exec()
        .then(doc => { 
            res.send(doc);
        })
        .catch(err => {
            console.log(err);
            res.send({error: err});
        });
}
module.exports.getMany = getMany;

// Update one computer
const update = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    const id = req.params.computer_id;

    const body = {
        brand: req.body.brand,
        model: req.body.model,
        processor: req.body.processor,
        processor_speed: req.body.processor_speed,
        screen_size: req.body.screen_size,
        screen_resolution: req.body.screen_resolution,
        ram: req.body.ram,
        hdd: req.body.hdd,
        ssd: req.body.ssd,
        graphics_card: req.body.graphics_card,
        operating_system: req.body.operating_system,
        available_location: req.body.available_location
    };

    computerModel.findByIdAndUpdate(id, body)
        .exec()
        .then(doc => {
            res.send({message: 'Computer Updated Successfully.'});
        })
        .catch(err => {
            console.log(err);
            res.send({error: err});
        })
}
module.exports.update = update;

// Remove one computer
const remove = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    const id = req.params.computer_id;

    computerModel.findByIdAndDelete(id)
        .exec()
        .then(doc => {
            res.send({message: 'Computer Deleted Successfully.'});
        })
        .catch(err => {
            console.log(err);
            res.send({error: err});
        });
}
module.exports.remove = remove;

// MapReduce to get number of each brand in the DB
const mapReduceBrandCount = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');

    let x = {};

    x.map = function() {emit(this.brand, 1)};
    x.reduce = function(k, v) {return v.length};

    computerModel.mapReduce(x)
        .then(doc => {
            res.send(doc);
        })
        .catch(err => {
            console.log(err);
            res.send({error: err});
        });
}
module.exports.mapReduceBrandCount = mapReduceBrandCount;
