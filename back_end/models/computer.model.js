// computer.model.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model
const computerSchema = new Schema({
    brand: String,
    model: String,
    processor: String,
    processor_speed: Number,
    screen_size: Number,
    screen_resolution: String,
    ram: Number,
    hdd: Number,
    ssd: Number,
    graphics_card: String,
    operating_system: String,
    available_location: {
        stock: Number,
        street: String,
        city: String,
        country: String,
        }
}, 
{
    timestamps: true
});

module.exports = mongoose.model('computer', computerSchema)
