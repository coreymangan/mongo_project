import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatTableModule,
         MatSnackBarModule, MatGridListModule, MatCardModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { UpdatePageComponent } from './pages/update-page/update-page.component';
import { ViewBrandPageComponent } from './pages/view-brand-page/view-brand-page.component';
import { FormContainerComponent } from './components/form-container/form-container.component';
import { TableContainerComponent } from './components/table-container/table-container.component';
import { ViewTableComponent } from './components/view-table/view-table.component';
import { ViewPageComponent } from './pages/view-page/view-page.component';
import { ApiService } from './shared/services/api.service';
import { ComputerService } from './shared/services/computer.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomePageComponent,
    CreatePageComponent,
    UpdatePageComponent,
    ViewBrandPageComponent,
    FormContainerComponent,
    TableContainerComponent,
    ViewTableComponent,
    ViewPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatSnackBarModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [ApiService, ComputerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
