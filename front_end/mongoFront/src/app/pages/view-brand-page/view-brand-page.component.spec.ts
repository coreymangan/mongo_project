import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBrandPageComponent } from './view-brand-page.component';

describe('ViewBrandPageComponent', () => {
  let component: ViewBrandPageComponent;
  let fixture: ComponentFixture<ViewBrandPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBrandPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBrandPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
