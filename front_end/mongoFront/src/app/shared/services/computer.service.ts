import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../../environments/environment';

/**
 *
 * @export
 * @class ComputerService
 */

@Injectable()
export class ComputerService {

    constructor(private apiService: ApiService) {

    }

    /**
     * Gets list of all computers
     * @memberof ComputerService
     */
    public getComputers() {
        return this.apiService.get(environment.endpoints.getcomputers);
    }

    /**
     * Gets a specified computer
     * @param computerId id of computer
     * @memberof ComputerService
     */
    public getComputer(computerId: string) {
        return this.apiService.get(`${environment.endpoints.getcomputer}${computerId}`);
    }

    /**
     * Creates computer
     * @param computer computer object
     * @memberof ComputerService
     */
    public createComputer(computer: object) {
        return this.apiService.post(environment.endpoints.createComputer, computer);
    }

    /**
     * Updates specified computer
     * @param computerId id of computer
     * @param computer computer object
     * @memberof ComputerService
     */
    public updateComputer(computerId: string, computer: object) {
        return this.apiService.put(`${environment.endpoints.updatecomputer}${computerId}`, computer);
    }

    /**
     * Deletes specified computer
     * @param computerId id of computer
     * @memberof ComputerService
     */
    public removeComputer(computerId: string) {
        return this.apiService.delete(`${environment.endpoints.removecomputer}${computerId}`);
    }

    /**
     * Gets map reduce of all brands and counts them
     * @memberof ComputerService
     */
    public getBrandsCount() {
        return this.apiService.get(`${environment.endpoints.numBrands}`);
    }
}
