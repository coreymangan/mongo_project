import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComputerService } from 'src/app/shared/services/computer.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.scss']
})
export class FormContainerComponent implements OnInit {
  computerId: string;
  computer: any;
  computerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private computerService: ComputerService, private route: ActivatedRoute,
    private router: Router) {

    // Getting Ids from url params
    this.route.params.subscribe(res => {
      this.computerId = res.computerId;
    });


    // create computer form
    this.computerForm = this.formBuilder.group({
      'brand': ['', Validators.required],
      'model': [''],
      'processor': [''],
      'processor_speed': [''],
      'screen_size': [''],
      'screen_resolution': [''],
      'ram': [''],
      'hdd': [''],
      'ssd': [''],
      'graphics_card': [''],
      'operating_system': [''],
      'available_location': this.formBuilder.group({
        'stock': [''],
        'street': [''],
        'city': [''],
        'country': ['']
      })
    });
  }

  ngOnInit() {
    if (this.computerId) {
      this.getComputer();
    }
  }

  // Gets computer from DB
  getComputer() {
    this.computerService.getComputer(this.computerId)
      .subscribe(res => {
        console.log(res);
        this.computer = res;

        // Update Computer Form
        this.computerForm = this.formBuilder.group({
          'brand': [this.computer.brand, Validators.required],
          'model': [this.computer.model],
          'processor': [this.computer.processor],
          'processor_speed': [this.computer.processor_speed],
          'screen_size': [this.computer.screen_size],
          'screen_resolution': [this.computer.screen_resolution],
          'ram': [this.computer.ram],
          'hdd': [this.computer.hdd],
          'ssd': [this.computer.ssd],
          'graphics_card': [this.computer.graphics_card],
          'operating_system': [this.computer.operating_system],
          'available_location': this.formBuilder.group({
            'stock': [this.computer.available_location.stock],
            'street': [this.computer.available_location.street],
            'city': [this.computer.available_location.city],
            'country': [this.computer.available_location.country]
          })
        });
      }, err => {
        console.log(err);
        alert(err);
      });

  }

  // Create computer
  createComputer() {
    console.log(this.computerForm.value);
    this.computerService.createComputer(this.computerForm.value)
      .subscribe(res => {
        console.log(res);
        alert('Computer Created Successfully');
      }, err => {
        console.log(err);
        alert(err);
      });
  }

  // Update computer
  updateComputer() {
    this.computerService.updateComputer(this.computerId, this.computerForm.value)
      .subscribe(res => {
        console.log(res);
        alert('Computer Updated Successfully');
      }, err => {
        console.log(err);
        alert(err);
      });
  }

}
