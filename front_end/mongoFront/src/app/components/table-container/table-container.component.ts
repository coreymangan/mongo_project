import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { ComputerService } from 'src/app/shared/services/computer.service';

@Component({
  selector: 'app-table-container',
  templateUrl: './table-container.component.html',
  styleUrls: ['./table-container.component.scss']
})
export class TableContainerComponent implements OnInit {
  computers: any;
  total_rows: Number;
  dataSource: MatTableDataSource<any>;
  displayedColumns: String[] = ['brand', 'model', 'processor', 'processor_speed', 'screen_size', 'screen_resolution', 'ram', 'hdd',
                                'ssd', 'graphics_card', 'operating_system', 'stock', 'street', 'city', 'country', 'update', 'remove'];

  constructor(private computerService: ComputerService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getComputers();
  }

  // Get Computers
  getComputers() {
    this.computerService.getComputers()
      .subscribe(res => {
        console.log(res);
        this.computers = res;
        this.total_rows = res.length;
        this.dataSource = new MatTableDataSource(this.computers);
      }, err => {
        console.log(err);
        alert(err);
      });
  }

  // Removes computer from db
  removeComputer(computerId: string) {
    this.computerService.removeComputer(computerId)
      .subscribe(res => {
        this.snackBar.open('Computer Removed', 'Close', {
          duration: 500
        });
        this.getComputers();
      }, err => {
        console.log(err);
        alert(err);
      });
  }


  // Searches for computer
  applyFilter(event: any) {
    console.log(event);
  }

}
