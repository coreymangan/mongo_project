import { Component, OnInit } from '@angular/core';
import { ComputerService } from 'src/app/shared/services/computer.service';
import { MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-view-table',
  templateUrl: './view-table.component.html',
  styleUrls: ['./view-table.component.scss']
})
export class ViewTableComponent implements OnInit {
  computers: any;
  total_rows: Number;
  dataSource: MatTableDataSource<any>;
  displayedColumns: String[] = ['brand', 'number'];

  constructor(private comuterService: ComputerService) { }

  ngOnInit() {
    this.getBrands();
  }

  // Gets computer brands and counts them
  getBrands() {
    this.comuterService.getBrandsCount()
      .subscribe(res => {
        console.log(res);
        this.computers = res.results;
        this.dataSource = new MatTableDataSource(this.computers);
        this.total_rows = res.stats.counts.output;
      }, err => {
        console.log(err);
        alert(err);
      });
  }

  // Searches for computer
  applyFilter(event: any) {
    console.log(event);
  }

}
