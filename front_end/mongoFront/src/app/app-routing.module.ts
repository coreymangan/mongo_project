import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { ViewPageComponent } from './pages/view-page/view-page.component';
import { UpdatePageComponent } from './pages/update-page/update-page.component';
import { ViewBrandPageComponent } from './pages/view-brand-page/view-brand-page.component';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'createcomputer',
    component: CreatePageComponent
  },
  {
    path: 'viewcomputers',
    component: ViewPageComponent
  },
  {
    path: 'updatecomputer/:computerId',
    component: UpdatePageComponent
  },
  {
    path: 'computerbrands',
    component: ViewBrandPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
